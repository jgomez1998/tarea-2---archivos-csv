package TallerArchivoNotas;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
public class NotasMAD {
    public static void main(String[] args) {
        Scanner teclado = new Scanner (System.in);
        int nro;
        double for1, chat1, vid1, tra1, pre1, for2, chat2, vid2, tra2, pre2, fin1 = 0, fin2 = 0, total, parcial1 = 0, parcial2 = 0;
        String Estudiante, alerta = " ", promocion = "Aprobado";
        try {
            Formatter fileOut = new Formatter("File1.csv");
            fileOut.format("%s;%s;%s;%s;%s;%s;%s;%s;%s", "\t", "\t", "\t", "\t", "\t", "\t", "\t", "\t", "NOTAS\r\n");
            fileOut.format("\r\n");
            fileOut.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", "NOMBRES", "FOR1", "CHAT1", "VID1", "TRA1", "PRE1", "FOR2", "CHAT2",
                    "VID2", "TRA2", "PRE2", "FIN1", "FIN2", "TOTAL", "ALERTA", "PROMOCION\r\n");
            System.out.print("Digite numero de estudiantes: "); nro = teclado.nextInt();
            for (int i = 0; i < nro; i++) {
                Estudiante = "Estudiante " + (i + 1) + ": ";
                for1 = NumeroAleatorios.Aleatorios(0, 1); 
                chat1 = NumeroAleatorios.Aleatorios(0, 1);
                vid1 = NumeroAleatorios.Aleatorios(0, 1);
                tra1 = NumeroAleatorios.Aleatorios(0, 6);
                pre1 = NumeroAleatorios.Aleatorios(0, 14);
                for2 = NumeroAleatorios.Aleatorios(0, 1);
                chat2 = NumeroAleatorios.Aleatorios(0, 1);
                vid2 = NumeroAleatorios.Aleatorios(0, 1);
                pre2 = NumeroAleatorios.Aleatorios(0, 14);
                tra2 = NumeroAleatorios.Aleatorios(0, 6);
                if (pre1 < 8) {
                    fin1 = NumeroAleatorios.Aleatorios(0, 20);
                    alerta = "Rendir final 1";
                    parcial1 = fin1;
                } else {
                    parcial1 = for1 + chat1 + vid1 + tra1 + pre1;
                }if (pre2 < 8) {
                    fin2 = NumeroAleatorios.Aleatorios(0, 20);;
                    alerta = "Rendir final 2";
                    parcial2 = fin2;
                } else {
                    parcial2 = for2 + chat2 + vid2 + tra2 + pre2;
                }if (pre1 < 8 && pre2 < 8) {
                    alerta = "Rendir final 1 y 2";
                }
                total = parcial1 + parcial2;
                if (total < 28) {
                    promocion = "Reprobado";
                }
                fileOut.format("%s;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%s;%s", Estudiante, for1, chat1, vid1, tra1,
                        pre1, for2, chat2, vid2, tra2, pre2, fin1, fin2, total, alerta, promocion);
                fin1 = fin2 = 0;
                alerta = " ";
                promocion = "Aprobado";
                fileOut.format("\r\n");
            }
            fileOut.close();
        } catch (FileNotFoundException e) {
        }
    }
}
class NumeroAleatorios {
    public static double Aleatorios(int minimo, int maximo) {
        double num = (double) (Math.random() * (minimo - (maximo + 1)) + (maximo + 1));
        return num;
    }
}
