package TallerArchivoNotas;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
public class NotasMAD2 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int nro;
        double cht1, asp1, for1, eva1, tar1, pre1, cht2, asp2, for2, eva2, tar2, pre2, total, acu60 = 0, exfin = 0, totfn = 0, acu40 = 0, recu = 0, fin = 0;
        String alt = null, Estudiante, est = null;
        double auxExamenes=0, totExamen=0, auxTar=0,totTarea=0,auxAprobado=0, totAprobado=0,auxReprobado =0;
        double totReprobado=0, totNotasmax=0, totNotamin=0,clasePromedio=0, totPromedioEst=0;
        try {
            Formatter fileOut = new Formatter("NotasMAD2.csv");
            fileOut.format("%s;%s;%s;%s;%s;%s;%s;%s;%s", "\t", "\t", "\t", "\t", "\t", "\t", "\t", "\t", "NOTAS\r\n");
            fileOut.format("\r\n");
            fileOut.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", "NOMBRES", "CTH1", "APS1", "FOR1", "EVA1", "TAR1", "PRE1", "CHT2",
                    "APS2", "FOR2", "EVA2", "TAR2", "PRE2", "TOTAL", "ALERTA", "ACU60", "EXFIN", "TOTFN", "ACU40", "RECU", "FIN", "EST\r\n");
            System.out.print("Digite numero de estudiantes: ");
            nro = teclado.nextInt();
            for (int i = 0; i < nro; i++) {
                Estudiante = "Estudiante " + (i + 1) + ": ";
                cht1 = NumeroAleatorios.Aleatorios(0, 0); asp1 = NumeroAleatorios.Aleatorios(0, 0);
                for1 = NumeroAleatorios.Aleatorios(0, 0); eva1 = NumeroAleatorios.Aleatorios(0, 1);
                tar1 = NumeroAleatorios.Aleatorios(0, 5); pre1 = NumeroAleatorios.Aleatorios(0, 9);
                cht2 = NumeroAleatorios.Aleatorios(0, 0); asp2 = NumeroAleatorios.Aleatorios(0, 0);
                for2 = NumeroAleatorios.Aleatorios(0, 0); eva2 = NumeroAleatorios.Aleatorios(0, 1);
                tar2 = NumeroAleatorios.Aleatorios(0, 5); pre2 = NumeroAleatorios.Aleatorios(0, 9);
                if (cht1 == 0 || cht2 == 0) {
                    asp1 = 1;
                    asp2 = 1;
                } else {
                    asp1 = 0;
                    asp2 = 0;
                }
                total = cht1 + asp1 + for1 + eva1 + tar1 + pre1 + cht2 + asp2 + for2 + eva2 + tar2 + pre2;
                if (total > 28) {
                    acu60 = 0; exfin = 0; totfn = 0; acu40 = 0; recu = 0; fin = 0;
                    est = "APROBADO";
                } else {
                    if (total < 28) {
                        alt = "RECUPERACION";
                        acu60 = total * 0.6;
                        exfin = NumeroAleatorios1.Aleatorios(0, 16);
                        totfn = acu60 + exfin;
                        if (totfn>28) {
                            est = "APROBADO";
                            acu40 = 0; recu = 0; fin = totfn;
                        }else{
                            acu40 = totfn * 0.4;
                            recu = NumeroAleatorios1.Aleatorios(0, 24);
                            fin = acu40 + recu;
                        }if (fin<28) {
                            est = "REPROBADO";
                        }else{
                            est = "APROBADO";
                        }
                    }
                }
                 if (pre1 == 0 || pre2 == 0 || exfin == 0 || recu == 0) {
                    auxExamenes += 1;
                    totExamen = (100*auxExamenes)/nro;
                }
                 if (tar1 == 0 || tar2 == 0) {
                    auxTar += 1;
                    totTarea = (100*auxTar)/nro;
                }
                  if ("APROBADO".equals(est)) {
                    auxAprobado += 1;
                    totAprobado = (100*auxAprobado)/nro;
                }
                if ("REPROBADO".equals(est)) {
                    auxReprobado += 1;
                    totReprobado = (100*auxReprobado)/nro;
                }  
                if (total >= 38) 
                    totNotasmax += 1;
                if (total >= 0 && total <= 27) 
                    totNotamin += 1;
                clasePromedio += total / 10;
                if (total > clasePromedio) 
                    totPromedioEst += 1;
                fileOut.format("%s;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%s;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%s",
                        Estudiante, cht1, asp1, for1, eva1, tar1, pre1, cht2, asp2, for2, eva2, tar2, pre2, total, alt, acu60, exfin, totfn, acu40, recu, fin, est);
                alt = " ";    
                est = "";
                fileOut.format("\r\n");
            }
            fileOut.format("\r\n%.2f;%s;", totExamen, "% De estudiantes no asistieron a sus examenes");
            fileOut.format("\r\n%.2f;%s;", totTarea, "% De estudiantes no presentaron sus tareas");
            fileOut.format("\r\n%.2f;%s;", totAprobado, "% De estudiantes aprobaron");
            fileOut.format("\r\n%.2f;%s;", totReprobado, "% De estudiantes reprobaron");
            fileOut.format("\r\n%.2f;%s;", totNotasmax, "Estudiantes estuvieron cerca de los 40 puntos");
            fileOut.format("\r\n%.2f;%s;", totNotamin, "Estudiantes tienen notas menores a la base ´28´ ");
            fileOut.format("\r\n%.2f;%s;",clasePromedio, "Es la media de la clase");
            fileOut.format("\r\n%.2f;%s;", totPromedioEst, "Estudiantes estan por encima de la media");
            fileOut.close();
        } catch (FileNotFoundException e) {
        }
    }
}
class NumeroAleatorios1 {
    public static double Aleatorios(int minimo, int maximo) {
        double num = (double) (Math.random() * (minimo - (maximo + 1)) + (maximo + 1));
        return num;
    }
}
